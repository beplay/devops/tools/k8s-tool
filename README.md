# kubectl tool for CI/CD

## Installed software

- kustomize-4.0.5
- kubectl-1.20
- jq-latest
- yq-4.6.3

## Build

```sh
IMAGE=<IMAGENAME>:<TAG>

docker build -t $IMAGE .
```

## Usage

Mount `kubconfig` file to container then set environment variable `KUBECONFIG` to `kubeconfig` file.

```sh

IMAGE=IMAGE=<IMAGENAME>:<TAG>


docker run -it --rm  \
  --name kubectl \
  -e KUBECONFIG=$KUBECONFIG \
  -v config:/tmp/kubeconfig \
  $IMAGE
```
